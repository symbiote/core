<h1><img src="http://symbiote.io/images/symbiote.png" width=36 height=36/>Symbiote</h1>

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://github.com/jpdillingham/Symbiote/blob/master/LICENSE)

## About

Symbiote is a cross-platform (via Mono) application platform written in C#.  The code in this repository is a work in progress and has not yet reached an Alpha version.

Constructive feedback/criticism is greatly appreciated but contributions to this repository will not be accepted at this time.  

## Documentation

Sporadically updated, Sandcastle-generated documentation can be found at [http://symbiote.io/doc](http://symbiote.io/doc).
