﻿/*
      █▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀ ▀▀▀  ▀  ▀      ▀▀ 
      █   
      █     ▄████████                                                                      ▄█                             
      █     ███    ███                                                                    ███                             
      █     ███    ███   ▄█████   ▄█████    ▄█████    ▄▄██▄▄▄  ▀██████▄   █       ▄█   ▄  ███▌ ██▄▄▄▄     ▄█████  ██████  
      █     ███    ███   ██  ▀    ██  ▀    ██   █   ▄█▀▀██▀▀█▄   ██   ██ ██       ██   █▄ ███▌ ██▀▀▀█▄   ██   ▀█ ██    ██ 
      █   ▀███████████   ██       ██      ▄██▄▄     ██  ██  ██  ▄██▄▄█▀  ██       ▀▀▀▀▀██ ███▌ ██   ██  ▄██▄▄    ██    ██ 
      █     ███    ███ ▀███████ ▀███████ ▀▀██▀▀     ██  ██  ██ ▀▀██▀▀█▄  ██       ▄█   ██ ███  ██   ██ ▀▀██▀▀    ██    ██ 
      █     ███    ███    ▄  ██    ▄  ██   ██   █   ██  ██  ██   ██   ██ ██▌    ▄ ██   ██ ███  ██   ██   ██      ██    ██ 
      █     ███    █▀   ▄████▀   ▄████▀    ███████   █  ██  █  ▄██████▀  ████▄▄██  █████  █▀    █   █    ██       ██████  
      █
 ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄  ▄▄ ▄▄   ▄▄▄▄ ▄▄     ▄▄     ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄ ▄ 
 █████████████████████████████████████████████████████████████ ███████████████ ██  ██ ██   ████ ██     ██     ████████████████ █ █ 
      ▄  
      █  Information about the assembly.
      █  
      █▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ ▀▀▀▀▀▀▀▀▀▀▀ ▀ ▀▀▀     ▀▀               ▀   
      █  The GNU Affero General Public License (GNU AGPL)
      █  
      █  Copyright (C) 2016 JP Dillingham (jp@dillingham.ws)
      █  
      █  This program is free software: you can redistribute it and/or modify
      █  it under the terms of the GNU Affero General Public License as published by
      █  the Free Software Foundation, either version 3 of the License, or
      █  (at your option) any later version.
      █  
      █  This program is distributed in the hope that it will be useful,
      █  but WITHOUT ANY WARRANTY; without even the implied warranty of
      █  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      █  GNU Affero General Public License for more details.
      █  
      █  You should have received a copy of the GNU Affero General Public License
      █  along with this program.  If not, see <http://www.gnu.org/licenses/>.
      █  
      ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀  ▀▀ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀██ 
                                                                                                   ██ 
                                                                                               ▀█▄ ██ ▄█▀ 
                                                                                                 ▀████▀   
                                                                                                   ▀▀                            */
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Symbiote")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Symbiote")]
[assembly: AssemblyCopyright("Copyright (C) 2016 JP Dillingham (jp@dillingham.ws)")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("899d0a77-77e8-4bcf-848e-2167c8cc036f")]
[assembly: AssemblyVersion("0.5.0.0")]
[assembly: AssemblyFileVersion("0.5.0.0")]
