﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbiote.Core.Event
{
    public class EventManagerConfiguration
    {
        public List<string> Events { get; set; }
    }
}
